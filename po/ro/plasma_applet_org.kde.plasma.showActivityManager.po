# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2011, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-20 00:47+0000\n"
"PO-Revision-Date: 2020-09-20 08:55+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Aspect"

#: package/contents/ui/ConfigAppearance.qml:24
#, kde-format
msgid "Icon:"
msgstr "Pictogramă:"

#: package/contents/ui/ConfigAppearance.qml:26
#, kde-format
msgid "Show the current activity icon"
msgstr "Arată pictograma activității actuale"

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgid "Show the generic activity icon"
msgstr "Arată pictograma generică de activitate"

#: package/contents/ui/ConfigAppearance.qml:42
#, kde-format
msgid "Title:"
msgstr "Titlu:"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Show the current activity name"
msgstr "Arată denumirea activității actuale"

#: package/contents/ui/main.qml:73
#, fuzzy, kde-format
#| msgid "Show the current activity icon"
msgctxt "@info:tooltip"
msgid "Current activity is %1"
msgstr "Arată pictograma activității actuale"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Show Activity Manager"
msgstr "Arată gestionarul de activități"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Click to show the activity manager"
msgstr "Apăsați pentru a afișa gestionarul de activități"

#~ msgid "Click or press the Meta key and 'Q' to show the activity manager"
#~ msgstr ""
#~ "Faceți clic sau apăsați tasta Meta și „Q” pentru a afișa gestionarul de "
#~ "activități"

#~ msgid ""
#~ "Click or press the associated keyboard shortcut (%1) to show the activity "
#~ "manager"
#~ msgstr ""
#~ "Faceți clic sau apăsați acceleratorul de tastatură asociat (%1) pentru a "
#~ "afișa gestionarul de activități"
