# translation of joystick.po to Gujarati
# Pragnesh Radadiya <pg.radadia@gmail.com>, 2008.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:48+0000\n"
"PO-Revision-Date: 2008-10-01 20:16+0530\n"
"Last-Translator: Pragnesh Radadiya <pg.radadia@gmail.com>\n"
"Language-Team: Gujarati <gu@li.org>\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Pragnesh Radadiya"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "pg.radadia@gmail.com"

#: caldialog.cpp:26 joywidget.cpp:335
#, kde-format
msgid "Calibration"
msgstr "આંકવુ"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "પછી"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "ચોક્કસાઇ નક્કી કરવા માટે થોડી ક્ષણ રાહ જુઓ"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(સામાન્ય રીતે X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(સામાન્ય રીતે Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt> આંકવુ એ ઉપકરણ કેટલા ગાળામાં મૂલ્ય આપે છે તે તપાસવા વિશેનુ છે.<br/><br/>મહેરબાની કરીને "
"તમારા ડીવાઇસ પર <b>%1%2  અક્ષીસને</b> <b>ઓછામાં ઓછા</b> સ્થાન પર ફેરવો.<br/><br/"
">ઉપકરણ પર ત્યારબાદના પગથિયાથી શરુ કરવા માટે કોઇપણ બટન દબાવો અથવા 'પછી' બટન "
"દબાવો.</qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt> આંકવુ એ ઉપકરણ કેટલા ગાળામાં મૂલ્ય આપે છે તે તપાસવા વિશેનુ છે.<br/><br/>મહેરબાની કરીને "
"તમારા ઉપકરણ પર <b>%1%2  અક્ષીસને</b> <b>મધ્યના</b> સ્થાન પર ફેરવો.<br/><br/"
">ડીવાઇસ પર ત્યારબાદના પગથિયાથી શરુ કરવા માટે કોઇપણ બટન દબાવો અથવા 'પછી' બટન "
"દબાવો.</qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt> આંકવુ એ ઉપકરણ કેટલા ગાળામાં મૂલ્ય આપે છે તે તપાસવા વિશેનુ છે.<br/><br/>મહેરબાની કરીને "
"તમારા ડીવાઇસ પર <b>%1%2  અક્ષીસને</b> <b>વધુમાં વધુ</b> સ્થાન પર ફેરવો.<br/><br/"
">ઉપકરણ પર ત્યારબાદના પગથિયાથી શરુ કરવા માટે કોઇપણ બટન દબાવો અથવા 'પછી' બટન "
"દબાવો.</qt>"

#: caldialog.cpp:160 joywidget.cpp:325 joywidget.cpp:361
#, kde-format
msgid "Communication Error"
msgstr "સંદેશાવ્યવહાર ક્ષતિ"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "તમે સફળતાપૂર્વક ઉપકરણને આંકયું છે"

#: caldialog.cpp:164 joywidget.cpp:363
#, kde-format
msgid "Calibration Success"
msgstr "આંકવામાં સફળતા"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "મૂલ્ય અક્ષીસ %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "આપેલ ઉપકરણ %1 ખોલી શકયા નથી: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "આપેલ ઉપકરણ %1 જોયસ્ટીક નથી."

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે કર્નલ ડ્રાઇવર વર્ઝન મેળવી શકયા નથી"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"અત્યારે ચાલતી કર્નલ ડ્રાઇવર વૃતાન્ત (%1 %2 %3) એ આ જ મોડયુલ નથી કે જેને માટે કંપાઇલ કરેલ "
"(%4 %5 %6)."

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે બટનનો આંકડો મેળવી શકયા નથી"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે અક્ષીસનો આંકડો મેળવી શકયા નથી"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે આંકેલા મૂલ્યો મેળવી શકયા નથી"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે આંકેલા મૂલ્યો પાછી લાગુ પાડી શકયા નથી"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે આંકેલા મૂલ્યો શરુઆત કરી શકયા નથી"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "જોયસ્ટીક ઉપકરણ %1: %2 માટે આંકેલા મૂલ્યો લાગુ પાડી ી શકયા નથી"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "આંતરિક ક્ષતિ - સંજ્ઞા %1 અજાણ્યી"

#: joystick.cpp:29
#, kde-format
msgid "KDE Joystick Control Module"
msgstr "કેડીઇ જોયસ્ટીક કાબૂ કરનાર મોડ્યુલ"

#: joystick.cpp:31
#, kde-format
msgid "KDE System Settings Module to test Joysticks"
msgstr "જોયસ્ટીક ચકાસવા માટે કેડીઇ સિસ્ટમ ગોઠવણી મોડ્યુલ "

#: joystick.cpp:33
#, kde-format
msgid "(c) 2004, Martin Koller"
msgstr "(c) ૨૦૦૪, માર્ટિન કોલેર"

#: joystick.cpp:38
#, kde-format
msgid ""
"<h1>Joystick</h1>This module helps to check if your joystick is working "
"correctly.<br />If it delivers wrong values for the axes, you can try to "
"solve this with the calibration.<br />This module tries to find all "
"available joystick devices by checking /dev/js[0-4] and /dev/input/"
"js[0-4]<br />If you have another device file, enter it in the combobox.<br /"
">The Buttons list shows the state of the buttons on your joystick, the Axes "
"list shows the current value for all axes.<br />NOTE: the current Linux "
"device driver (Kernel 2.4, 2.6) can only autodetect<ul><li>2-axis, 4-button "
"joystick</li><li>3-axis, 4-button joystick</li><li>4-axis, 4-button "
"joystick</li><li>Saitek Cyborg 'digital' joysticks</li></ul>(For details you "
"can check your Linux source/Documentation/input/joystick.txt)"
msgstr ""
"<h1>જોયસ્ટીક</h1>આ મોડ્યુલ તમને તપાસવામાં મદદ કરે છે કે જોયસ્ટીક વ્યવસ્થિત રીતે કામ કરે છે."
"<br />જો તેને અક્ષીસ પર ખોટા મૂલ્યો પહોચાડયા હશે, તો તમે તેને આંકેલથી તેને ઉકેલી શકો છો. "
"<br />આ મોડ્યુલ /dev/js[0-4] અને /dev/input/js[0-4] માં તપાસીને જોયસ્ટીક શોધવા "
"પ્રયત્ન કરે છે <br /> જો તમારી પાસે કોઇ બીજી કોઇ ઉપકરણ ફાઇલ હોય, કોમ્બોબોક્ષ માં લખી "
"શકો છો.<br /> બટનોની યાદી જોયસ્ટીક પરના બટનોની સ્થિતિ બતાવે છે, અક્ષીસની યાદી "
"હાલના બધી અક્ષીસનુ મૂલ્ય સૂચવે છે.<br />નોંધ: હાલનુ લિનક્ષ ઉપકરણ ડ્રાઇવર (કર્નલ ૨.૪,૨.૬) "
"જ માત્ર જાતઓળખાણ કાઢી શકે છે<ul><li>૨-અક્ષીસ, ૪-બટન જોયસ્ટીક</li><li>૩-અક્ષીસ, ૪-"
"બટન જોયસ્ટીક</li><li>૪-અક્ષીસ, ૪-બટન જોયસ્ટીક</li><li>સાઇટેક સાઇર્બોગ જોયસ્ટીકો</"
"li></ul>(માહિતી માટે તમે તપાસી શકો છો Linux source/Documentation/input/"
"joystick.txt"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "ઉપકરણ:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "સ્થાન:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "ટ્રેસ બતાવો"

#: joywidget.cpp:96 joywidget.cpp:300
#, kde-format
msgid "PRESSED"
msgstr "દબાવેલ છે"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "બટનો:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "સ્થિતિ"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "અક્ષીસ:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "મૂલ્ય"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "આંકેલુ"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"આ ગણકયંત્ર પર કોઇપણ જોયસ્ટીક ઉપકરણ જાતે મળયું નથી.<br/>/dev/js[0-4] અને /dev/input/"
"js[0-4] તપાસવામાં આવેલ છે.<br/>જો તમે જાણતા હોય કે એક લગાડેલ છે તો, મહેરબાની કરીને "
"સાચી ઉપકરણ ફાઇલ દાખલ કર."

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"આપેલ ઉપકરણ નામ બરાબર નથી (તેમાં /dev નો સામાવેશ થયો નથી).\n"
"ઉપકરણને યાદીમાંથી પસંદ કરો અથવા\n"
"ઉપકરણ ફાઇલ લખો જેમ કે, /dev/js0."

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "અજાણ્યુ ઉપકરણ"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "ઉપકરણ ક્ષતિ"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:331
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>આંકવુ એ ચોક્કસાઇ તપાસવા વિશેનુ છે.<br/><br/><b>મહેરબાની કરીને બધા અક્ષીસને તેની "
"મધ્યમ સ્થિતિ પર લાવો અને ત્યારબાદ જોયસ્ટીકને પકડો જ નહી.</b><br/><br/>આંકવાની શરુઆત "
"કરવા બરાબર પર કલીક કરો.</qt>"

#: joywidget.cpp:363
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "જોયસ્ટીક ઉપકરણ %1 માટે પાછા લાગુ પાડેલ બધા આંકેલા મૂલ્યો."
