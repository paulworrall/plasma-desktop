# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2014, 2016.
# Vit Pelcak <vpelcak@suse.cz>, 2017, 2019, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2022-02-16 10:11+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr "Pozice na obrazovce:"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Top"
msgstr "Nahoře"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Center"
msgstr "Střed"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr "Aktivace:"

#: package/contents/ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "History:"
msgstr "Historie:"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr ""

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr ""

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr ""

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "Vyprázdnit historii"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Clear History…"
msgstr "Vyprázdnit historii…"

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr ""

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr ""

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr ""
