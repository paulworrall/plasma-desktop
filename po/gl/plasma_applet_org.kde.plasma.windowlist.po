# translation of plasma_applet_windowlist.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2012, 2013.
# Xosé <xosecalvo@gmail.com>, 2009.
# Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_windowlist\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-21 00:46+0000\n"
"PO-Revision-Date: 2016-04-21 06:54+0100\n"
"Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail."
"com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr ""

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr ""

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr ""

#: contents/ui/main.qml:80
#, kde-format
msgid "Plasma Desktop"
msgstr ""

#~ msgid "Show list of opened windows"
#~ msgstr "Mostrar unha lista coas xanelas abertas"

#~ msgid "On all desktops"
#~ msgstr "En todos os escritorios"

#~ msgid "Window List"
#~ msgstr "Lista de xanelas"

#~ msgid "Actions"
#~ msgstr "Accións"

#~ msgid "Unclutter Windows"
#~ msgstr "Xanelas que non se interfiran"

#~ msgid "Cascade Windows"
#~ msgstr "Xanelas en cascata"

#~ msgctxt "%1 is the name of the desktop"
#~ msgid "Desktop %1"
#~ msgstr "Escritorio %1"

#~ msgid "Current desktop"
#~ msgstr "Escritorio actual"

#~ msgid "No windows"
#~ msgstr "Sen xanelas"
