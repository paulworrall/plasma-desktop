# translation of joystick.po to Greek
# translation of joystick.po to
#
# Spiros Georgaras <sngeorgaras@otenet.gr>, 2005, 2006, 2007, 2008.
# Toussis Manolis <koppermind@yahoo.com>, 2005.
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2005, 2007.
# Spiros Georgaras <sng@hellug.gr>, 2007.
# Petros <pvidalis@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:48+0000\n"
"PO-Revision-Date: 2010-02-18 16:03+0200\n"
"Last-Translator: Petros <pvidalis@gmail.com>\n"
"Language-Team: Ελληνικά <i18ngr@lists.hellug.gr>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Σπύρος Γεωργαράς, Τούσης Μανώλης, Πέτρος Βιδάλης"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"sngeorgaras@otenet.gr,manolis@koppermind.homelinux.org, p_vidalis@hotmail.com"

#: caldialog.cpp:26 joywidget.cpp:335
#, kde-format
msgid "Calibration"
msgstr "Ρύθμιση αξόνων"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "Επόμενο"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "Παρακαλώ περιμένετε για τον υπολογισμό ακριβείας"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(συνήθως το X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(συνήθως το Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Η ρύθμιση αξόνων πρόκειται να ελέγξει το εύρος τιμών που λειτουργεί η "
"συσκευή σας. <br /> <br />Παρακαλώ μετακινήστε τον <b> άξονα %1 %2</b> στη "
"συσκευή σας στην <b>ελάχιστη</b>θέση. <br /><br />Πιέστε κάποιο κουμπί στη "
"συσκευή για συνέχεια στο επόμενο βήμα.</qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Η ρύθμιση αξόνων πρόκειται να ελέγξει το εύρος τιμών που λειτουργεί η "
"συσκευή σας. <br /> <br />Παρακαλώ μετακινήστε τον <b> άξονα %1 %2</b> στη "
"συσκευή σας στην <b>κεντρική</b>θέση. <br /><br />Πιέστε κάποιο κουμπί στη "
"συσκευή για συνέχεια στο επόμενο βήμα.</qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Η ρύθμιση αξόνων πρόκειται να ελέγξει το εύρος τιμών που λειτουργεί η "
"συσκευή σας. <br /> <br />Παρακαλώ μετακινήστε τον <b> άξονα %1 %2</b> στη "
"συσκευή σας στη <b>μέγιστη</b>θέση. <br /><br />Πιέστε κάποιο κουμπί στη "
"συσκευή για συνέχεια στο επόμενο βήμα.</qt>"

#: caldialog.cpp:160 joywidget.cpp:325 joywidget.cpp:361
#, kde-format
msgid "Communication Error"
msgstr "Σφάλμα επικοινωνίας"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "Ρυθμίσατε με Επιτυχία τη συσκευή σας"

#: caldialog.cpp:164 joywidget.cpp:363
#, kde-format
msgid "Calibration Success"
msgstr "Επιτυχία ρύθμισης"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "Τιμή άξονα %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "Η δοσμένη συσκευή %1 δεν μπορεί να ανοιχτεί: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "Η δοσμένη συσκευή %1 Δεν είναι χειριστήριο."

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr ""
"Δεν μπορεί να βρεθεί η έκδοση του οδηγού στον πυρήνα για το χειριστήριο %1: "
"%2"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"Η τρέχουσα έκδοση πυρήνα (%1.%2.%3) δεν αντιστοιχεί στην έκδοση του οδηγού "
"(%4.%5.%6)."

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "Δεν μπορεί να διαβαστεί ο αριθμός κουμπιών του χειριστηρίου %1: %2"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "Δεν μπορεί να διαβαστεί ο αριθμός αξόνων του χειριστηρίου %1: %2"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "Δεν μπορούν να εξαχθούν τιμές ρύθμισης για το χειριστήριο %1: %2"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr "Δεν μπορούν να επανέλθουν οι τιμές ρύθμισης του χειριστηρίου %1: %2"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr ""
"Δεν μπορούν να αρχικοποιηθούν οι τιμές ρύθμισης του χειριστηρίου %1: %2"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "Δεν μπορούν να εφαρμοστούν οι τιμές ρύθμισης για το χειριστήριο %1: %2"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "Εσωτερικό σφάλμα - Κωδικός %1 άγνωστος"

#: joystick.cpp:29
#, kde-format
msgid "KDE Joystick Control Module"
msgstr "Άρθρωμα Ρύθμισης Χειριστηρίου του KDE"

#: joystick.cpp:31
#, kde-format
msgid "KDE System Settings Module to test Joysticks"
msgstr "Άρθρωμα ρυθμίσεων συστήματος KDE για τον έλεγχο χειριστηρίων"

#: joystick.cpp:33
#, kde-format
msgid "(c) 2004, Martin Koller"
msgstr "(c) 2004, Martin Koller"

#: joystick.cpp:38
#, kde-format
msgid ""
"<h1>Joystick</h1>This module helps to check if your joystick is working "
"correctly.<br />If it delivers wrong values for the axes, you can try to "
"solve this with the calibration.<br />This module tries to find all "
"available joystick devices by checking /dev/js[0-4] and /dev/input/"
"js[0-4]<br />If you have another device file, enter it in the combobox.<br /"
">The Buttons list shows the state of the buttons on your joystick, the Axes "
"list shows the current value for all axes.<br />NOTE: the current Linux "
"device driver (Kernel 2.4, 2.6) can only autodetect<ul><li>2-axis, 4-button "
"joystick</li><li>3-axis, 4-button joystick</li><li>4-axis, 4-button "
"joystick</li><li>Saitek Cyborg 'digital' joysticks</li></ul>(For details you "
"can check your Linux source/Documentation/input/joystick.txt)"
msgstr ""
"<h1>Χειριστήριο</h1>Το άρθρωμα αυτό βοηθά στον έλεγχο καλής λειτουργίας του "
"χειριστηρίου σας.<br />Αν αποδίδει λάθος τιμές στους άξονες, μπορείτε να "
"δοκιμάσετε την επίλυσή του με τη Ρύθμιση αξόνων.<br />Το άρθρωμα αυτό "
"προσπαθεί να αναζητήσει όλα τα χειριστήρια ελέγχοντας τις /dev/js[0-4] και /"
"dev/input/js[0-4]<br />Αν έχετε κάποιο άλλο όνομα συσκευής, εισάγετέ το στο "
"πλαίσιο.<br />Η λίστα κουμπιών δείχνει την κατάσταση του κουμπιών του "
"χειριστηρίου, Η λίστα αξόνων δείχνει την τρέχουσα τιμή για όλους τους άξονες."
"<br />ΣΗΜΕΙΩΣΗ: Ο τρέχων οδηγός πυρήνα (Kernel 2.4, 2.6) μπορεί να "
"αναγνωρίσει<ul><li>Χειριστήριο με 2-άξονες, 4-κουμπιά</li><li>Χειριστήριο με "
"3-άξονες, 4-κουμπιά</li><li>Χειριστήριο με 4-άξονες, 4-κουμπιά</"
"li><li>Ψηφιακά Χειριστήρια Saitek Cyborg </li></ul> (Περισσότερες "
"πληροφορίες στο Linux source/Documentation/input/joystick.txt)"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "Συσκευή:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "Θέση:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "Εμφάνιση ίχνους"

#: joywidget.cpp:96 joywidget.cpp:300
#, kde-format
msgid "PRESSED"
msgstr "ΠΑΤΗΜΕΝΟ"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "Κουμπιά:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "Κατάσταση"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "Άξονες:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "Τιμή"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "Ρύθμιση"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"Δε βρέθηκε κανένα χειριστήριο στον υπολογιστή σας.<br />Ελέγχθηκαν τα /dev/"
"js[0-4] και /dev/input/js[0-4]<br />Αν είστε σίγουροι ότι είναι κάποιο "
"συνδεδεμένο, παρακαλώ δώστε το σωστό αρχείο συσκευής."

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"Το αρχείο συσκευής είναι εσφαλμένο ( δεν περιέχει /dev).\n"
"Παρακαλώ επιλέξτε συσκευή από τη λίστα ή\n"
"δώστε ένα όνομα συσκευής, όπως /dev/js0."

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "Άγνωστη συσκευή"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "Σφάλμα συσκευής"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:331
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>Η Ρύθμιση πρόκειται να ελέγξει την ακρίβεια της συσκευής. <br /> <br /"
"><b>Παρακαλώ μετακινήστε όλους τους άξονες στο κέντρο και έπειτα αφήστε το "
"χειριστήριο ελεύθερο. </b><br /><br />Πιέστε Εντάξει για να ξεκινήσετε τη "
"ρύθμιση.</qt>"

#: joywidget.cpp:363
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "Όλες οι τιμές ρύθμισης του χειριστηρίου %1 Επανήλθαν."
